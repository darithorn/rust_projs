mod img;

#[cfg(test)]
mod tests {
    use super::img;
    #[test]
    fn lshift() {
        assert_eq!(1, img::lshift(&[1, 0, 0, 0]));
        assert_eq!(168364039, img::lshift(&[7, 8, 9, 10]));
        // 00001010
        // 00001001
        // 00001000
        // 00000111
        // 00001010000010010000100000000111
    }
    
    #[test]
    fn bmp_open() {
        let bmp = img::Bitmap::open("res/test.bmp");
        assert_eq!(bmp.bits, 24);
        assert_eq!(bmp.gap1, 0);
        assert_eq!(bmp.get_pixel(6, 6), 0xFF);
    }
}
