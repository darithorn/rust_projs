use std::fs::File;
use std::io::Read;
use std::path::Path;

pub fn lshift(bytes: &[u8]) -> i32 {
    // shifts from left to right
    // first index isn't shifted (least significant)
    // last index is shifted most to left (most significant)
    let mut sig = 0;
    let mut result: i32 = 0;
    for i in 0..bytes.len() {
        result |= (bytes[i] as i32) << sig;
        sig += 8;
    }
    result
    // (bytes[0] as i32) | (bytes[1] as i32) << 8 | 
    // (bytes[2] as i32) << 16 | (bytes[3] as i32) << 24
}

fn take_bytes(file: &File, n: usize) -> Vec<u8> {
    file.bytes().take(n).map(|x| x.unwrap()).collect()
}

pub struct Bitmap {
    pub width: i32,
    pub height: i32,
    pub bits: i32, // bits per pixel
    pub data: Box<Vec<u8>>,
    pub gap1: i32
}

impl Bitmap {
    pub fn open(filename: &str) -> Bitmap {
        let filepath = Path::new(&filename);
        let f = &match File::open(filepath) {
            Ok(f) => f,
            Err(e) => panic!("{}", e)
        };
        // Bitmap File Header
        take_bytes(f, 10);
        let bmp_header_offset = lshift(take_bytes(f, 4).as_slice()); // address offset
        // DIB Header
        let bmp_dib_size = lshift(take_bytes(f, 4).as_slice()); // DIB size
        let width = lshift(take_bytes(f, 4).as_slice()); // width in pixels
        let height = lshift(take_bytes(f, 4).as_slice()); // height in pixels
        // color panes
        if lshift(&take_bytes(f, 2)) != 1 {
            panic!("malformed bmp file - color panes not equal to 1");
        }
        let bmp_dib_bits = lshift(take_bytes(f, 2).as_slice()); // bits per pixel
        let bmp_dib_compression = lshift(take_bytes(f, 4).as_slice());
        take_bytes(f, (bmp_dib_size - 20) as usize); // retrieve rest of DIB Header
        // BI_BITFIELDS or BI_ALPHABITFIELDS compression methods have Extra bit masks
        // TODO(darithorn): is this correct?
        let mut bytes_read = bmp_dib_size + 14; // used to calculate Gap1
        if bmp_dib_compression == 3 { // BI_BITFIELDS
            take_bytes(f, 12);
            bytes_read += 12;
        } else if bmp_dib_compression == 6 { // BI_ALPHABITFIELDS
            take_bytes(f, 16);
            bytes_read += 16;
        }
        // TODO(darithorn): add support for color tables
        if bmp_dib_bits <= 8 {
            panic!("not supported - bits per pixel is <= 8");
        }
        // TODO(darithorn): figure out how to deal with Gap1
        let gap1 = bmp_header_offset - bytes_read;
        if gap1 < 0 {
            panic!("gap1 is not supposed to be negative");
        }
        take_bytes(f, gap1 as usize);
        let bmp_pixel_data = Box::new(take_bytes(f, (width * height) as usize));
        Bitmap { width: width, height: height, bits: bmp_dib_bits, data: bmp_pixel_data, gap1: gap1 }
    }
    
    pub fn get_pixel(&self, x: i32, y: i32) -> i32 {
        let mut pixels: Vec<u8> = vec![0, 0, 0, 0];
        let offset: usize = (x * self.width + y) as usize;
        pixels[0] = self.data[offset];
        if self.bits >= 16 {
            pixels[1] = self.data[offset + 1];
        }
        if self.bits >= 24 {
            pixels[2] = self.data[offset + 2];
        }
        if self.bits >= 32 {
            pixels[3] = self.data[offset + 3];
        }
        lshift(pixels.as_slice())
    }
}