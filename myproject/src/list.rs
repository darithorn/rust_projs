pub struct List<T> {
    head: Option<Box<Node<T>>>,
}

struct Node<T> {
    next: Option<Box<Node<T>>>,
    data: T
}

impl<T> List<T> {
    pub fn new() -> Self {
        List { head: None }
    }
    
    pub fn push(&mut self, data: T) {
        let new = Node {
            next: self.head.take(),
            data: data
        };
        self.head = Some(Box::new(new));
    }
    
    pub fn push_list(&mut self, list: Self) {
        self.head = list.head
    }
    
    pub fn pop(&mut self) -> Option<T> {
        self.head.take().map(|node| {
            let node = *node;
            self.head = node.next;
            node.data 
        })
    }
}

#[cfg(test)]
mod list_test {        
    use super::List;
    #[test]    
    fn basic() {
        let mut list = List::new();
        assert_eq!(list.pop(), None);
        
        list.push(1);
        list.push(2);
        list.push(3);
        assert_eq!(list.pop(), Some(3));
        assert_eq!(list.pop(), Some(2));
        
        list.push(4);
        list.push(5);
        assert_eq!(list.pop(), Some(5));
        assert_eq!(list.pop(), Some(4));
        assert_eq!(list.pop(), Some(1));
        assert_eq!(list.pop(), None);
    }
}