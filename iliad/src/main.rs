use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::str::Chars;

enum Token {
    Word(String),
    Number(String),
    Special(char)
}

impl Token {
    fn pretty_print(self) {
        match self {
            Token::Word(s) => println!("{}", s),
            Token::Number(n) => println!("{}", n),
            Token::Special(c) => println!("{}", c)
        }
    }
}

struct Lexer<'a> {
    pos: usize,
    buffer: &'a Vec<char>,
    errors: Vec<String>
}

impl<'a> Lexer<'a> {
    fn error(&mut self, msg: String) {
        self.errors.push(msg);
    }
    
    fn peek(&mut self) -> Option<char> {
        let c = self.read_char();
        self.pos -= 1;
        c
    }
    
    fn unread_char(&mut self) {
        self.pos -= 1;
    }
    
    fn read_char(&mut self) -> Option<char> {
        // None is equiv to EOF
        if self.pos >= self.buffer.len() { None }
        else {
            let result = Some(self.buffer[self.pos]);
            self.pos += 1;
            result
        }
    }
    
    fn read_word(&mut self, c: char) -> Token {
        let mut s = String::new();
        s.push(c);
        loop {
            let opt_ch = self.read_char();
            match opt_ch {
                None => break,
                Some(ch) => {
                    if ch.is_alphabetic() {
                        s.push(ch);  
                    } else {
                        // we want to unread_char the character
                        self.unread_char();
                        break;
                    }
                }
            }
        }
        Token::Word(s)
    }
    
    fn read_number(&mut self, c: char) -> Token {
        let mut s = String::new();
        let decimal = false;
        s.push(c);
        loop {
            let opt_ch = self.read_char();
            match opt_ch {
                None => break,
                Some(ch) => {
                    if ch.is_digit(10) {
                        s.push(ch);
                    } else if ch == '.' {
                        if decimal {
                            self.error("too many decimals");
                        }
                        decimal = true;
                    } else {
                        self.unread_char();
                        break;
                    }
                }
            }
        }
        Token::Number(s)
    }
}

fn main() {
    let path = Path::new("res/test.il");
    let display = path.display();
    let mut file = match File::open(&path) {
        Err(err) => panic!("{}: {}", display, err.description()),
        Ok(file) => file,
    };
    let mut contents = String::new();
    match file.read_to_string(&mut contents) {
        Err(err) => panic!("{}: {}", display, err.description()),
        Ok(_) => {}
    }
    let mut char_vec: Vec<char> = contents.chars().collect();
    let mut state = Lexer { pos: 0, buffer: &char_vec };    
    loop {
        let ch = state.read_char();
        match ch {
            None => break,            
            Some(c) => {
                if c.is_alphabetic() {
                    let t = state.read_word(c);
                    t.pretty_print();
                }
            }
        }
    }
}